#!/bin/bash
#echo "enter CM host"
#read -p "CM Host  : " CM_HOST
#read -p "User Name[used for cluster login]  : " USER
CM_HOST=`cat /etc/cloudera-scm-agent/config.ini | grep "server_host" |cut -d'=' -f2 | xargs`
USER_s=`whoami`
echo "Hello Welcome to Smoketest "
[ -d /tmp/smoketests/ ] && rm -rf /tmp/smoketests/
mkdir /tmp/smoketests/
curl -u $USER_s -k https://$CM_HOST:7183/api/v19/cm/kerberosPrincipals > /tmp/smoketests/krb_export.txt
curl -u $USER_s -k https://$CM_HOST:7183/api/v19/cm/deployment?view=EXPORT > /tmp/smoketests/cm_export.txt
while :
do
  echo "                       #################################################"
  echo "                       ####  Please enter the choice and hit Enter  ####"
  echo "                       #################################################"
  echo "                       1.   hdfs "
  echo "                       2.   impala-shell"
  echo "                       3.   hive-beeline"
  echo "                       4.   mapreduce job"
  echo "                       5.   spark submit"
  echo "                       6.   UDF Functions"
  echo "                       7.   Send output through mail"
  echo "                       8.   Run all checks & send mail"
  echo "                       9.   Exit"
  read INPUT_NUM
  case $INPUT_NUM in
        1)
                echo "hdfs dfs -ls /"
                hdfs dfs -ls / | tee /tmp/smoketests/hdfs_test.txt
                echo "#################################################################################################"
                ;;
        2)
                echo "impala-shell test"
                IMP_Load_b=`cat /tmp/smoketests/cm_export.txt | python -mjson.tool |  grep -A2 "impalad_load_balancer" | sed 's/"//g'|sed 's/,//g' | grep "value" | cut -d':' -f2 | awk 'NR==1{print $1}' | xargs`
                IMP_D=`cat /tmp/smoketests/krb_export.txt | python -mjson.tool | grep impala | sed 's/"//g'|sed 's/,//g' | cut -d'/' -f2 |cut -d'@' -f1 | awk 'NR==1{print $1}'`
                [[ ! -z "$IMP_Load_b" ]] && impala-shell -i $IMP_Load_b -d default -k --ssl --ca_cert=/opt/cloudera/security/pki/x509/truststore.pem -q "show databases;" | tee /tmp/smoketests/impala_test.txt || impala-shell -i $IMP_D -d default -k --ssl --ca_cert=/opt/cloudera/security/pki/x509/truststore.pem -q "show databases;" | tee /tmp/smoketests/impala_test.txt
                echo "#################################################################################################"
                ;;
        3)
                echo "hive-beeline"
                HIV_Load_b=`cat /tmp/smoketests/cm_export.txt | python -mjson.tool |  grep -A2 "hiverserver2_load_balancer" | sed 's/"//g'|sed 's/,//g' | grep "value" | cut -d':' -f2 | xargs`
                HIVE_Host=`cat /tmp/smoketests/krb_export.txt | python -mjson.tool | grep hive | sed 's/"//g'|sed 's/,//g' | cut -d'/' -f2 |cut -d'@' -f1 | awk 'NR==1{print $1}'`
                REALM=`cat /tmp/smoketests/krb_export.txt | python -mjson.tool | grep hive | sed 's/"//g'|sed 's/,//g' | cut -d'/' -f2 |cut -d'@' -f2 | awk 'NR==1{print $1}'`
                [[ ! -z "$HIV_Load_b" ]] && beeline -u "jdbc:hive2://$HIV_Load_b:10000/default;ssl=true;principal=hive/_HOST@$REALM" -e "show databases;" | tee /tmp/smoketests/hive_test.txt || beeline -u "jdbc:hive2://$HIVE_Host:10000/default;ssl=true;principal=hive/_HOST@$REALM" -e "show databases;" | tee /tmp/smoketests/hive_test.txt
                echo "#################################################################################################"
                ;;
        4)
                echo "mapreduce job"
                hadoop jar /opt/cloudera/parcels/CDH/lib/hadoop-mapreduce/hadoop-mapreduce-examples.jar pi 10 10 | tee  /tmp/smoketests/MR_test.txt
                echo "#################################################################################################"
                ;;
        5)
                echo "spark-submit"
                 spark-submit --class org.apache.spark.examples.SparkPi --master yarn --deploy-mode cluster /opt/cloudera/parcels/CDH/lib/spark/examples/jars/spark-examples_*.jar 10 | tee /tmp/smoketests/spark_submit_test.txt
                echo"#################################################################################################"
                ;;
        6)
                echo "impala-shell test"
                IMP_Load_b=`cat /tmp/smoketests/cm_export.txt | python -mjson.tool |  grep -A2 "impalad_load_balancer" | sed 's/"//g'|sed 's/,//g' | grep "value" | cut -d':' -f2 | awk 'NR==1{print $1}' | xargs`
                IMP_D=`cat /tmp/smoketests/krb_export.txt | python -mjson.tool | grep impala | sed 's/"//g'|sed 's/,//g' | cut -d'/' -f2 |cut -d'@' -f1 | awk 'NR==1{print $1}'`
                [[ ! -z "$IMP_Load_b" ]] && impala-shell -i $IMP_Load_b -d default -k --ssl --ca_cert=/opt/cloudera/security/pki/x509/truststore.pem -q "show databases;" | tee /tmp/smoketests/impala_test.txt || impala-shell -i $IMP_D -d default -k --ssl --ca_cert=/opt/cloudera/security/pki/x509/truststore.pem -q "show databases;" | tee /tmp/smoketests/impala_test.txt
                echo "#################################################################################################"
                ;;
        7)
                echo "Sending mail"
                SMTP_host=`cat /tmp/smoketests/cm_export.txt | python -mjson.tool |  grep -A2 "alert_mailserver_hostname" | sed 's/"//g'|sed 's/,//g' | grep "value" | cut -d':' -f2 | xargs`
                ls /tmp/smoketests/*_test.txt >/tmp/smoketests/list
                mailx -a `pr -$(wc -l </tmp/smoketests/list) -tJS' -a ' -W1000 /tmp/smoketests/list` -S smtp=`echo $SMTP_host` -s "Smoke-tests output" -v msharma@phdata.io < /dev/null
                ;;
        8)
                echo "Running all the tests at once!!!"
                echo "hdfs dfs -ls /"
                hdfs dfs -ls / | tee /tmp/smoketests/hdfs_test.txt
                echo "impala-shell test"
                IMP_D=`cat /tmp/smoketests/krb_export.txt | python -mjson.tool | grep impala | sed 's/"//g'|sed 's/,//g' | cut -d'/' -f2 |cut -d'@' -f1 | awk 'NR==1{print $1}'`
                impala-shell -i $IMP_D -d default -k --ssl --ca_cert=/opt/cloudera/security/pki/x509/truststore.pem -q "show databases;" | tee /tmp/smoketests/impala_test.txt
                echo "#################################################################################################"
                echo "hive-beeline"
                HOST_NAME=`cat /tmp/smoketests/krb_export.txt | python -mjson.tool | grep hive | sed 's/"//g'|sed 's/,//g' | cut -d'/' -f2 |cut -d'@' -f1 | awk 'NR==1{print $1}'`
                REALM=`cat /tmp/smoketests/krb_export.txt | python -mjson.tool | grep hive | sed 's/"//g'|sed 's/,//g' | cut -d'/' -f2 |cut -d'@' -f2 | awk 'NR==1{print $1}'`
                beeline -u "jdbc:hive2://$HOST_NAME:10000/default;ssl=true;principal=hive/_HOST@$REALM" -e "show databases;" | tee /tmp/smoketests/hive_test.txt
                echo"#################################################################################################"
                echo "mapreduce job"
                hadoop jar /opt/cloudera/parcels/CDH/lib/hadoop-mapreduce/hadoop-mapreduce-examples.jar pi 10 10 | tee  /tmp/smoketests/MR_test.txt
                echo"#################################################################################################"
                echo "spark-submit"
                spark-submit --class org.apache.spark.examples.SparkPi --master yarn --deploy-mode cluster /opt/cloudera/parcels/CDH/lib/spark/examples/jars/spark-examples_*.jar 10 | tee /tmp/smoketests/spark_submit_test.txt
                echo"#################################################################################################"
                echo "UDF Functions"
                IMP_D=`cat /tmp/smoketests/krb_export.txt | python -mjson.tool | grep impala | sed 's/"//g'|sed 's/,//g' | cut -d'/' -f2 |cut -d'@' -f1 | awk 'NR==1{print $1}'`
                impala-shell -i $IMP_D -d ezpin -k --ssl --ca_cert=/opt/cloudera/security/pki/x509/truststore.pem -q "SHOW FUNCTIONS;" | tee /tmp/smoketests/UDF_test.txt
                echo"#################################################################################################"
                echo "Sending mail"
                SMTP_host=`cat /tmp/smoketests/cm_export.txt | python -mjson.tool |  grep -A2 "alert_mailserver_hostname" | sed 's/"//g'|sed 's/,//g' | grep "value" | cut -d':' -f2 | xargs`
                ls /tmp/smoketests/*_test.txt >/tmp/smoketests/list
                mailx -a `pr -$(wc -l </tmp/smoketests/list) -tJS' -a ' -W1000 /tmp/smoketests/list` -S smtp=`echo $SMTP_host` -s "Smoke-tests output" -v msharma@phdata.io < /dev/null
                ;;
        9)
                echo "Exiting !!!"
                exit
                ;;
        *)
                echo "Sorry !!! I didn't get you, Please enter correct choice"
                break
                ;;
  esac
done
echo
rm -rf /tmp/smoketests/
echo "########################################################################################################"

